Herramienta de Auditoria de Redes con diversos ataques. 
Este es el codigo para el ataque de ingenieria social conocido como gemelo siniestro. 

Falta eliminar deauth para implementarlo en una rama del Deauther V3 simplificada. 
Arreglar el html (script kiddie here).
Probar configuraciones de antena (test fisico).

Solo permite seleccionar una red para el ataque. El deauther y generador de handshakes
igual solo permitiran elegir una red. Para bloquear /duplicar multiples SSID usar el deauther de Space. 

La idea es un protoboard con multiples herramientas de ataque 802.11n.
Con un par de ESP32, pila y un script loader fisico. 

Compilado y probado en NODEMCU. 

Credito: Spacehuhn, M1z23R, NicoHood
